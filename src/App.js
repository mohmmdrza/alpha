import React, { Component } from 'react';
import { Switch, Route, Redirect, HashRouter } from 'react-router-dom';
import './App.css';
import Main from './components/main';
import Album from './components/album';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-v4-rtl/dist/js/bootstrap.min.js';
import 'bootstrap-v4-rtl/dist/css/bootstrap-rtl.css';
import Blog from './components/blog';
import Users from './components/users';
import UserData from './components/user_data';

class App extends Component {
  render() {
    return (
      <div className="container-fluid">
        <HashRouter>
          <Switch>
            <Route exact path="/">
              <Redirect to="/main" />
            </Route>
            <Route exact path="/main" component={Main}/>
            <Route exact path="/album" component={Album}/>
            <Route exact path="/blog" component={Blog}/>
            <Route exact path="/users" component={Users}/>
            <Route path="/user/:id" component={UserData}/>
          </Switch>
        </HashRouter>
      </div>
    );
  }
}

export default App;
