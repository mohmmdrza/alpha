import React, { Component } from 'react';
import SidebarMenu from './sidebar_menu';
import { get } from '../api/network';

class Main extends Component {

    state = {
        posts: [],
        images: []
    }

    componentDidMount = () => {
        get('https://jsonplaceholder.typicode.com/posts', (data) => {
            this.setState({
                posts: data
            })
        }, () => {})
        get('https://jsonplaceholder.typicode.com/photos', (data) => {
            this.setState({
                images: data
            }, () => {})
        })
    }

    render() {
        const { images , posts } = this.state;
        return (
            <div className="d-flex flex-row justify-content-between align-items-start w-100">
                <div className="d-flex flex-row w-25">
                    <SidebarMenu menuPosition={'1'}/>
                </div>
                <div style={{width: '72%', backgroundColor: 'gray', borderRadius: '8px', padding: '2%'}}>
                    <div className="d-flex flex-row justify-content-between w-100">
                        <div style={{width: '48%', textAlign: 'center'}}>
                            <div style={{fontSize: '20px' , fontWeight: 'bold', marginBottom: '4%'}}>last picture</div>
                            <img style={{width: '96%'}} src={images.length > 0 ? images[images.length -1].url : null} alt={'images'}/>
                        </div>
                        <div style={{width: '48%', textAlign: 'center'}}>
                            <div style={{fontSize: '20px' , fontWeight: 'bold', marginBottom: '4%'}}>last post</div>
                            <div>
                                <div style={{fontWeight: 'bold', marginBottom: '4%'}}>{posts.length > 0 ? posts[posts.length -1].title : null}</div>
                                <div>{posts.length > 0 ? posts[posts.length -1].body : null}</div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default Main;


