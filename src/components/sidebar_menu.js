import React, { Component } from 'react';

class SidebarMenu extends Component {
    render() {
        const { menuPosition } = this.props; 
        return(
            <div className="sidebarMenu">
                <div className={menuPosition == '1' ? "sideItemsActive" : "sideItems"}><a href="#/main">main page</a></div>
                <div className={menuPosition == '2' ? "sideItemsActive" : "sideItems"}><a href="#/album">album</a></div>
                <div className={menuPosition == '3' ? "sideItemsActive" : "sideItems"}><a href="#/blog">blog</a></div>
                <div className={menuPosition == '4' ? "sideItemsActive" : "sideItems"}><a href="#/users">users</a></div>
            </div>
        );
    }
}


export default SidebarMenu;