import React, { Component } from 'react';
import SidebarMenu from './sidebar_menu';
import { get } from '../api/network';
import ReactPaginate from 'react-paginate';

class Blog extends Component {

    state = {
        posts: [],
        users: [],
        currentPost: 0
    }

    componentDidMount = () => {
        get('https://jsonplaceholder.typicode.com/posts', (data) => {
            this.setState({
                posts: data
            })
        }, () => {})
        get('https://jsonplaceholder.typicode.com/users', (data) => {
            this.setState({
                users: data
            }, () => {})
        })
    }

    handleUser = (id) => {
        for(let i = 0; i< this.state.users.length; i++) {
            if(id == this.state.users[i].id) {
                return (
                    <div style={{color: 'white', fontWeight: 'bolder'}}>{this.state.users[i].name}</div>
                )
            }
        }
    }

    render() {
        const { posts, currentPost } = this.state;
        return (
            <div className="d-flex flex-row justify-content-between align-items-start w-100">
                <div className="d-flex flex-row w-25">
                    <SidebarMenu menuPosition={'3'}/>
                </div>
                <div style={{width: '72%', backgroundColor: 'gray', borderRadius: '8px', padding: '2%'}}>
                    <div className="gridFiveColumns w-100">
                        {posts.map((item, index) => (
                            index < (currentPost + 1) * 10 && index >= (currentPost) * 10 ?
                                <div className="w-100" style={{border: '1px solid black'}}>
                                    <div style={{width: '100%', textAlign: 'center', marginBottom: '2%'}}>
                                        {this.handleUser(item.userId)}
                                    </div>
                                    <div style={{width: '100%', textAlign: 'center'}}>
                                        <div>
                                            <div style={{marginBottom: '4%'}}>{item.title}</div>
                                        </div>
                                    </div>
                                </div>
                            :   null
                        ))}
                        
                    </div>
                    <div className="d-flex flex-row" style={{marginTop: '2%'}}>
                        <ReactPaginate
                        containerClassName="myProPagination"
                        pageCount={Math.ceil(posts.length/10)}
                        disableInitialCallback={true}
                        onPageChange={(num) => this.setState({currentPost: num.selected})}
                        previousLabel={null}
                        nextLabel={null}
                        breakClassName="myProPaginationLi"
                        pageRangeDisplayed={2}
                        activeClassName="activeLiMyPro"/>
                    </div>
                </div>
            </div>
        )
    }
}

export default Blog;


