import React, { Component } from 'react';
import SidebarMenu from './sidebar_menu';
import { get } from '../api/network';
import ReactPaginate from 'react-paginate';

class Album extends Component {

    state = {
        albums: [],
        images: [],
        users: [],
        currentImage: 0,
    }

    componentDidMount = () => {
        get('https://jsonplaceholder.typicode.com/users', (data) => {
            this.setState({
                users: data
            })
        }, () => {})
        get('https://jsonplaceholder.typicode.com/photos', (data) => {
            this.setState({
                images: data
            }, () => {})
        })
        get('https://jsonplaceholder.typicode.com/albums', (data) => {
            this.setState({
                albums: data
            }, () => {})
        })
    }

    render() {
        const { images , users, albums, currentImage } = this.state;
        return (
            <div className="d-flex flex-row justify-content-between align-items-start w-100">
                <div className="d-flex flex-row w-25">
                    <SidebarMenu menuPosition={'2'}/>
                </div>
                <div style={{width: '72%', backgroundColor: 'gray', borderRadius: '8px', padding: '2%'}}>
                    <div style={{marginBottom: '2%'}}>
                        <div className="gridFiveColumns w-100">
                            {images.map((item, index) => (
                                index < (currentImage + 1) * 10 && index >= (currentImage) * 10 ?
                                <div className="w-100" style={{border: '1px solid black', padding: '2%'}}>
                                    <div style={{width: '100%', textAlign: 'center'}}>
                                        <div>
                                            <img src={item.url} alt={"image"} style={{width: '100%'}}/>
                                            {albums.map(albumItem => 
                                                (albumItem.id == item.albumId ?
                                                    <>
                                                    <div style={{color: 'darkred', fontSize: '16px'}}>{albumItem.title}</div>
                                                    {users.map(userItem => (
                                                        userItem.id == albumItem.userId ? 
                                                            <>
                                                                <div style={{color: 'white', fontWeight: 'bolder', fontSize: '10px'}}>name: {userItem.name}</div>
                                                                <div style={{color: 'white', fontWeight: 'bolder', fontSize: '10px'}}>username: {userItem.username}</div>
                                                                <div style={{color: 'white', fontWeight: 'bolder', fontSize: '10px'}}>email: {userItem.email}</div>
                                                            </>
                                                        : null
                                                    ))}
                                                    </>
                                                : 
                                            null))}
                                        </div>
                                        <div>
                                        </div>
                                    </div>
                                </div>
                                : null
                            ))}
                        </div>
                        <div className="d-flex flex-row" style={{marginTop: '2%'}}>
                            <ReactPaginate
                            containerClassName="myProPagination"
                            pageCount={Math.ceil(images.length/10)}
                            disableInitialCallback={true}
                            onPageChange={(num) => this.setState({currentImage: num.selected})}
                            previousLabel={null}
                            nextLabel={null}
                            breakClassName="myProPaginationLi"
                            pageRangeDisplayed={2}
                            activeClassName="activeLiMyPro"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Album;


