import React, { Component } from 'react';
import SidebarMenu from './sidebar_menu';
import { get } from '../api/network';

class Users extends Component {

    state = {
        users: [],
    }

    componentDidMount = () => {
        get('https://jsonplaceholder.typicode.com/users', (data) => {
            this.setState({
                users: data
            }, () => {})
        })
    }

    render() {
        const { users } = this.state;
        return (
            <div className="d-flex flex-row justify-content-between align-items-start w-100">
                <div className="d-flex flex-row w-25">
                    <SidebarMenu menuPosition={'4'}/>
                </div>
                <div style={{width: '72%', backgroundColor: 'gray', borderRadius: '8px', padding: '2%'}}>
                    <div className="gridFiveColumns w-100">
                        {users.map(item => (
                            <div onClick={() => window.location.assign(`#/user/${item.id}`)} className="w-100" style={{border: '1px solid black', margin: '4%', padding: '8%'}}>
                                <div style={{width: '100%', textAlign: 'center', marginBottom: '2%'}}>
                                    <img src={require('../assets/avatar.png')} alt="avatar"/>
                                </div>
                                <div style={{width: '100%', textAlign: 'center'}}>
                                    <div>
                                        <div style={{marginBottom: '4%', fontWeight: 'bold'}}>{item.username}</div>
                                    </div>
                                </div>
                            </div>
                        ))}
                        
                    </div>
                    
                </div>
            </div>
        )
    }
}

export default Users;


