import React, { Component } from 'react';
import SidebarMenu from './sidebar_menu';
import { get } from '../api/network';
import ReactPaginate from 'react-paginate';

class UserData extends Component {

    state = {
        posts: [],
        currentPost: 0,
        users: [],
        albums: [],
        currentAlbum: 0,
    }

    componentDidMount = () => {
        get(`https://jsonplaceholder.typicode.com/posts?userId=${this.props.match.params.id}`, (data) => {
            console.log(data)
            this.setState({
                posts: data
            })
        }, () => {})
        get('https://jsonplaceholder.typicode.com/users', (data) => {
            this.setState({
                users: data
            }, () => {})
        })
        get('https://jsonplaceholder.typicode.com/albums', (data) => {
            this.setState({
                albums: data.filter(el => el.userId == this.props.match.params.id)
            }, () => {})
        })
    }

    handleUser = (id) => {
        for(let i = 0; i< this.state.users.length; i++) {
            if(id == this.state.users[i].id) {
                return (
                    <>
                        <div style={{color: 'white', fontWeight: 'bolder'}}>name: {this.state.users[i].name}</div>
                        <div style={{color: 'white', fontWeight: 'bolder'}}>username: {this.state.users[i].username}</div>
                        <div style={{color: 'white', fontWeight: 'bolder'}}>email: {this.state.users[i].email}</div>
                    </>
                )
            }
        }
    }

    render() {
        const { posts, albums, currentAlbum, currentPost } = this.state;
        return (
            <div className="d-flex flex-row justify-content-between align-items-start w-100">
                <div className="d-flex flex-row w-25">
                    <SidebarMenu menuPosition={'4'}/>
                </div>
                <div style={{width: '72%', backgroundColor: 'gray', borderRadius: '8px', padding: '2%'}}>
                    <div style={{marginBottom: '2%'}}>
                        <div style={{marginBottom: '1%', fontSize: '20px', fontWeight: 'bold'}}>User details:</div>
                        {this.handleUser(this.props.match.params.id)}
                    </div>
                    <div style={{marginBottom: '2%'}}>
                        <div style={{marginBottom: '1%', fontSize: '20px', fontWeight: 'bold'}}>Posts:</div>
                        <div className="gridFiveColumns w-100">
                            {posts.map((item, index) => (
                                index < (currentPost + 1) * 10 && index >= (currentPost) * 10 ?
                                <div className="w-100" style={{border: '1px solid black'}}>
                                    <div style={{width: '100%', textAlign: 'center'}}>
                                        <div>
                                            <div style={{marginBottom: '4%'}}>{item.title}</div>
                                        </div>
                                    </div>
                                </div>
                                : null
                            ))}
                        </div>
                        <div className="d-flex flex-row" style={{marginTop: '2%'}}>
                            <ReactPaginate
                            containerClassName="myProPagination"
                            pageCount={Math.ceil(posts.length/10)}
                            disableInitialCallback={true}
                            onPageChange={(num) => this.setState({currentPost: num.selected})}
                            previousLabel={null}
                            nextLabel={null}
                            breakClassName="myProPaginationLi"
                            pageRangeDisplayed={2}
                            activeClassName="activeLiMyPro"/>
                        </div>
                    </div>
                    <div style={{marginBottom: '2%'}}>
                        <div style={{marginBottom: '1%', fontSize: '20px', fontWeight: 'bold'}}>Albums:</div>
                        <div className="gridFiveColumns w-100">
                            {albums.map((item, index) => (
                                index < (currentAlbum + 1) * 10 && index >= (currentAlbum) * 10 ?
                                    <div className="w-100" style={{border: '1px solid black'}}>
                                        <div style={{width: '100%', textAlign: 'center'}}>
                                            <div>
                                                <div style={{marginBottom: '4%'}}>{item.title}</div>
                                            </div>
                                        </div>
                                    </div>
                                : null
                            ))}
                        </div>
                        <div className="d-flex flex-row" style={{marginTop: '2%'}}>
                            <ReactPaginate
                            containerClassName="myProPagination"
                            pageCount={Math.ceil(albums.length/10)}
                            disableInitialCallback={true}
                            onPageChange={(num) => this.setState({currentAlbum: num.selected})}
                            previousLabel={null}
                            nextLabel={null}
                            breakClassName="myProPaginationLi"
                            pageRangeDisplayed={2}
                            activeClassName="activeLiMyPro"/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default UserData;


