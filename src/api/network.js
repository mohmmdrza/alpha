import Axios from "axios";

const wrapRequest = (request, onSuccess, onFailed) => {
    return request.then((response) => {
        if(response.status === 200) {
            onSuccess(response.data);
        } else onFailed();
    }).catch((reason) => onFailed(reason));
}
const getConfig = () => {
    return {
        headers: {
            'Accept': 'application/json',
        }
    }
}


export const get = (url, onSuccess, onFailed) => {
    wrapRequest(Axios.get(url, getConfig()), onSuccess, onFailed);
}
